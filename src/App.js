import { useState } from "react";
import "./App.css";
import HeaderSearch from "./Components/HeaderSearch";
import axios from "axios";
import WeatherDetails from "./Components/WeatherDetails";

const base_url = "https://api.openweathermap.org/data/2.5/";
const apiId = "f825344b0cf0672c689378549f9868db";
function App() {
  const [searchValue, setSearchValue] = useState("");
  const [weatherDetails, setWeatherDetails] = useState(null);
  const searchLocation = (event) => {
    if (event.key === "Enter") {
      axios
        .get(
          `${base_url}weather?q=${searchValue}&appid=${apiId}&lang=fr&units=metric`
        )
        .then((response) => {
          setWeatherDetails(response.data);
          console.log(response.data);
        });
      setSearchValue("");
    }
  };
  console.log("searchValue", searchValue);
  return (
    <div className="App">
      <HeaderSearch
        setSearchValue={setSearchValue}
        searchValue={searchValue}
        searchLocation={searchLocation}
      />
      {weatherDetails && <WeatherDetails weatherDetails={weatherDetails} />}
    </div>
  );
}

export default App;
