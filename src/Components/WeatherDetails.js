import React from "react";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import humidityIcon from "../assets/humidity.png";
import PressureIcon from "../assets/barometer.png";
import windIcon from "../assets/wind.png";

function WeatherDetails({ weatherDetails }) {
  return (
    <div className="cardContent">
      <CardContent>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent={"space-around"}
          alignItems="center"
        >
          <Box p={1}>
            <Typography variant="h3" color="textPrimary">
              {weatherDetails?.name},{weatherDetails?.sys?.country}
            </Typography>
            <Typography variant="caption" color="textSecondary">
              {weatherDetails?.coord?.lon}, {weatherDetails?.coord?.lat}
            </Typography>
          </Box>
          <Box>
            <Box display="flex" flexDirection="row-reverse">
              <Box p={0}>
                {/* <Typography variant="h4" color="textPrimary">
                  Temp:
                </Typography> */}
                <Typography variant="h4" color="textSecondary">
                  {weatherDetails?.main?.temp}
                  <sup>
                    <span>&#176;</span>
                    {"C"}
                  </sup>
                </Typography>
              </Box>
            </Box>
            <Box display="flex" flexDirection="row-reverse">
              <Box p={0}>
                <Typography variant="h6" color="textSecondary">
                  {weatherDetails?.weather[0]?.description}
                </Typography>
              </Box>
            </Box>
          </Box>
          <Box p={1}>
            <img
              src={`http://openweathermap.org/img/w/${weatherDetails?.weather[0]?.icon}.png`}
              alt="weather-icon"
              className="weatherIcon"
            />
          </Box>
        </Box>
      </CardContent>
      <CardContent></CardContent>
      <CardContent>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent={"space-around"}
          alignItems="center"
        >
          <Box
            p={1}
            display="flex"
            flexDirection={"column"}
            justifyContent={"center"}
            alignItems="center"
          >
            <img src={humidityIcon} alt="humidityIcon" width={50} />
            <Typography variant="h6" color="textPrimary">
              Humidity
            </Typography>
            <Typography variant="h6" color="textSecondary">
              {weatherDetails?.main?.humidity} %
            </Typography>
          </Box>
          <Box
            p={1}
            display="flex"
            flexDirection={"column"}
            justifyContent={"center"}
            alignItems="center"
          >
            <img src={PressureIcon} alt="PressureIcon" width={50} />

            <Typography variant="h6" color="textPrimary">
              Pressure
            </Typography>
            <Typography variant="h6" color="textSecondary">
              {weatherDetails?.main?.pressure} pa
            </Typography>
          </Box>
          <Box
            p={1}
            display="flex"
            flexDirection={"column"}
            justifyContent={"center"}
            alignItems="center"
          >
            {" "}
            <img src={windIcon} alt="windIcon" width={50} />
            <Typography variant="h6" color="textPrimary">
              Wind
            </Typography>
            <Typography variant="h6" color="textSecondary">
              {weatherDetails?.wind?.speed} km/h
            </Typography>
          </Box>
        </Box>
      </CardContent>
    </div>
  );
}

export default WeatherDetails;
